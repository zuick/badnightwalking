﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeVolume : MonoBehaviour
{

	public AudioSource audioSource;
	public float DelayTime;
	public float NewVolume;

	public void OnEnable()
	{
		float startVolume = audioSource.volume;
		if (startVolume != NewVolume)
		{
			StartCoroutine(FadeOutSound());
		}
	}

	public IEnumerator FadeOutSound()
	{
		var delta = NewVolume - audioSource.volume;
		while (Mathf.Abs(audioSource.volume - NewVolume) > 0.01f)
		{
			audioSource.volume += delta / DelayTime * Time.deltaTime;
			yield return null;
		}
		audioSource.volume = NewVolume;
	}
}
