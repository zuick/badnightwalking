﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitiateSight : MonoBehaviour {

	// Use this for initialization
	void Start () {
		var target = transform.gameObject.GetComponentInChildren<CollidersDetector>();
		if (target)
		{
			target.enabled = true;
		}
	}

}
