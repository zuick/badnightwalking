using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

public class HitDetector : MonoBehaviour
{
	public Dictionary<int, Collision> collisions = new Dictionary<int, Collision>();
	private HitDetectorAgent[] agents;

	void Awake()
	{
		agents = GetComponentsInChildren<HitDetectorAgent>();
		foreach (var a in agents)
			a.OnHit.AddListener(OnCollision);
	}

	void OnDestroy()
	{
		foreach (var a in agents)
			a.OnHit.RemoveListener(OnCollision);
	}

	void OnCollision(Collision coll)
	{
		if(!collisions.ContainsKey(coll.collider.gameObject.layer))
			collisions.Add(coll.collider.gameObject.layer, coll);
	}

	void LateUpdate()
	{
		if (collisions.Count > 0)
			collisions.Clear();
	}
}