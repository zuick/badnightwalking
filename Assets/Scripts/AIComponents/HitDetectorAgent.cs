using UnityEngine;
using UnityEngine.Events;
using System;

public class HitDetectorAgent : MonoBehaviour {
	public LayerMask LayerMask;
	public bool destroySelfOnCollision;
	public UnityCollisionEvent OnHit;

	void OnCollisionEnter(Collision coll)
	{
		var otherLayer = coll.collider.gameObject.layer;
		if (LayerMask.IsMatch(otherLayer) && OnHit != null)
		{
			OnHit.Invoke(coll);
            if (destroySelfOnCollision)
            {
                var hitDetector = GetComponentInParent<HitDetector>();
                if(hitDetector)
                    Destroy(hitDetector.gameObject);
                else
                    Destroy(gameObject);
            }
		}
	}
}