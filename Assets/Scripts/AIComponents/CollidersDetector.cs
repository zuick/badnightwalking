using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;

public class CollidersDetector : MonoBehaviour
{
	public LayerMask sightLayerMask;
	public LayerMask obstaclesLayerMask;
	public string[] tagsFilter;
	public float distance = 6f;
	/*
	[Range(0, 360)]
	public float range = 45f;
	 */

	public List<Collider> visibleColliders;
	public float memoryTime = 1f;

	private float dotProductRange;
	private float maxObstacleCheckDistance = 1000f;

	/*
	private void OnEnable()
	{
		dotProductRange = range / 720f;
	}
	 */

	private void Update()
	{

		var colliders = Physics.OverlapSphere(transform.position, distance, sightLayerMask);

		if (memoryTime == 0f)
			visibleColliders.Clear();

		var newVisibleColliders = new List<Collider>();

		foreach (var c in colliders)
		{
			if (tagsFilter.Length > 0 && tagsFilter.FirstOrDefault(t => t == c.tag) == null)
				continue;

			var vectorToCollider = Vector3.Normalize(c.transform.position - transform.position);
			var forward = transform.right;
			
			/*
			var dotProduct = Vector2.Dot(vectorToCollider, forward);
			if (dotProduct < 1f - dotProductRange)
				continue;
			 */

			var colliderDistance = (transform.position - c.transform.position).magnitude;

			var obstacleHits = Physics.RaycastAll(
				transform.position,
				vectorToCollider, maxObstacleCheckDistance, obstaclesLayerMask
			);

			if (obstacleHits.Length > 0 && obstacleHits[0].collider != null && (obstacleHits[0].point - transform.position).magnitude < colliderDistance)
				continue;

			newVisibleColliders.Add(c);
		}

		var collidersToRemove = visibleColliders.Where(c => !newVisibleColliders.Contains(c)).ToList();
		var collidersToAdd = newVisibleColliders.Where(c => !visibleColliders.Contains(c)).ToList();

		foreach (var c in collidersToRemove)
		{
			if (memoryTime > 0f)
				StartCoroutine(RemoveFromVisible(c));
			else
				visibleColliders.Remove(c);
		}

		foreach (var c in collidersToAdd)
			visibleColliders.Add(c);
	}

	IEnumerator RemoveFromVisible(Collider c)
	{
		yield return new WaitForSeconds(memoryTime);
		visibleColliders.Remove(c);
	}

	public Collider GetDetected(LayerMask layerMask)
	{
		return visibleColliders.FirstOrDefault(c => layerMask.IsMatch(c.gameObject.layer));
	}
}