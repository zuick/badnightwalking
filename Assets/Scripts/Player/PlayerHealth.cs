using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
	public int Value
	{
		get
		{
			return value;
		}
	}
	public int maxValue = 100;
	public int enemyDamage = 10;
	public float restoreInterval = 3f;
	public int restoreDelta = 1;

	private int value;
	private float timer = 0f;

	private void Awake() {
		value = maxValue;
	}
	
	public void Add(int delta)
	{
		value = Mathf.Clamp(value + delta, 0, maxValue);
	}

	public void OnEnemyCollision(Collision other)
	{
		Add(-enemyDamage);
		if (value == 0 && LevelController.Instance != null)
			LevelController.Instance.ReloadScene();
	}

	private void Update()
	{
		if (value == maxValue || value == 0) return;

		timer += Time.deltaTime;
		if (timer > restoreInterval)
		{
			timer = 0f;
			Add(restoreDelta);
		}
	}
}