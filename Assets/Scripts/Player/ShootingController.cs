﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShootingMode {
    Infinite,
    Finite
}

public class ShootingController : MonoBehaviour
{
    public ShootingMode mode = ShootingMode.Infinite;
    public GameObject bulletPrefab;
    public Transform bulletPivot;

    public float bulletSpeed = 12f;
    public float cooldown;

    public int Bullets
    {
        get { return bullets; }
    }

    private bool busy = false;
    private int bullets = 0;
	
	private void Update()
	{
        if (Input.GetButtonDown("Fire") && !busy && (mode == ShootingMode.Infinite || bullets > 0))
		{
			Shoot();
		}
	}

	private void Shoot()
	{
		if(Time.timeScale == 0) return;
		
		var camera = Camera.main;
		if (!camera) return;

		var bullet = Instantiate(bulletPrefab, bulletPivot.position, bulletPivot.rotation);
		var bulletBody = bullet.GetComponent<Rigidbody>();
		
		if (bulletBody)
			bulletBody.velocity = camera.transform.forward.normalized * bulletSpeed;

        if (mode == ShootingMode.Finite) bullets--;

		StartCoroutine(ReleaseBusy());
	}

	IEnumerator ReleaseBusy()
	{
		busy = true;
		yield return new WaitForSeconds(cooldown);
		busy = false;
	}

    public void AddBullets(int count)
    {
        bullets += count;
    }
}
