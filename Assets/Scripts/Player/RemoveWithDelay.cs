using UnityEngine;

public class RemoveWithDelay : MonoBehaviour
{
	public GameObject target;
	public float delay;

	void OnEnable()
	{
		Destroy(target == null ? gameObject : target, delay);
	}
}