using UnityEngine;

public class MessageReader : MonoBehaviour
{
	public LayerMask messageLayer;

	void OnTriggerEnter(Collider coll)
	{
		var otherLayer = coll.gameObject.layer;
		if (!messageLayer.IsMatch(otherLayer)) return;

		var messageHolder = coll.gameObject.GetComponent<MessageHolder>();
		if (messageHolder == null) return;

		var messageWindow = WindowsManager.Instance.GetWindow("MessageWindow");
		
		if(messageWindow == null) return;
		messageWindow.Build(messageHolder.Read());
	}
}