using UnityEngine;
using UnityStandardAssets.ImageEffects;
using System;
using System.Collections;
using Game.Actions;

public class EnemyReaction : MonoBehaviour
{
	public VignetteAndChromaticAberration chromaticAberration;
	public float chromaticAberrationGain = 3f;
	public float vignetteGain = 3f;
	public float fadeOutTime = 1f;

	public float reactionForce = 10f;
	public Rigidbody body;

	public Vector3 deathZoneAngularSpeed;

	private float chromaticAbeerationOldValue;
	private float vignetteOldValue;

	private void Awake()
	{
		if (body == null)
			body = GetComponent<Rigidbody>();

		chromaticAbeerationOldValue = chromaticAberration.chromaticAberration;
		vignetteOldValue = chromaticAberration.intensity;
	}

	public void OnEnemyCollision(Collision other)
	{
		var direction = transform.position - other.transform.position;
		body.AddForce(direction * reactionForce, ForceMode.Impulse);

		StartCoroutine(DoAberrationFadeOut());
		StartCoroutine(DoVignetteFadeOut());
	}

	public void OnDeathZoneCollision(Collision other)
	{
		var fpsController = GetComponent<FPSController>();
		if (fpsController) fpsController.enabled = false;

		body.isKinematic = true;
		var mf = gameObject.AddComponent<MoveForwardAndTurn>();
		mf.angularSpeed = deathZoneAngularSpeed;
		mf.body = body;
		mf.sourceTransform = transform;
		mf.resetXRotation = false;
		mf.speed = 1f;


		if (Fader.instance != null)
		{
			Fader.instance.FadeIn();
			Fader.instance.OnFadeIn += ReloadScene;
		}
		else
			ReloadScene();
	}

	void ReloadScene()
	{
		if(LevelController.Instance == null) return;
			LevelController.Instance.ReloadScene();
	}

	IEnumerator DoAberrationFadeOut()
	{
		chromaticAberration.chromaticAberration = chromaticAbeerationOldValue;
		chromaticAberration.chromaticAberration *= chromaticAberrationGain;
		var dc = chromaticAberration.chromaticAberration - chromaticAbeerationOldValue;

		while (chromaticAberration.chromaticAberration > chromaticAbeerationOldValue)
		{
			chromaticAberration.chromaticAberration -= dc / fadeOutTime * Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
	}

	IEnumerator DoVignetteFadeOut()
	{
		chromaticAberration.intensity = vignetteOldValue;
		chromaticAberration.intensity *= vignetteGain;
		var dc = chromaticAberration.intensity - vignetteOldValue;

		while (chromaticAberration.intensity > vignetteOldValue)
		{
			chromaticAberration.intensity -= dc / fadeOutTime * Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
	}
}