using UnityEngine;
using System.Collections;

namespace Game.Actions
{
	[AddComponentMenu("AI State Actions/ObjectActivator")]
	public class ObjectActivator : MonoBehaviour
	{
		public GameObject targetObject;
		public bool inverse;
		public bool restore = true;

		protected virtual void OnEnable()
		{
			TargetObjectSetActive(true);
		}

		protected void TargetObjectSetActive(bool active)
		{
			if (targetObject == null)
				return;

			targetObject.SetActive(active ^ inverse);
		}

		void OnDisable()
		{
			if(restore)
				TargetObjectSetActive(false);
		}
	}
}