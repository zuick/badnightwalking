using UnityEngine;

namespace Game.Actions
{
	[AddComponentMenu("AI State Actions/RotateOnEnable")]
	public class RotateOnEnable : MonoBehaviour
	{
		public Transform target;
		public int period = 1;
		public Vector3 rotation;

		int counter = 0;
		private Vector3 perviousRotation;

		private void OnEnable() {
			if(counter % period == 0)
			{
				perviousRotation = target.rotation.eulerAngles;
				target.Rotate(perviousRotation + rotation);
			}
			counter++;
		}
	}
}