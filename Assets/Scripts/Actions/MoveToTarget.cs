using UnityEngine;
using UnityEngine.Events;

namespace Game.Actions
{
	[AddComponentMenu("AI State Actions/MoveToTarget")]
	public class MoveToTarget : MonoBehaviour
	{
		public Transform sourceTransform;
		public float speed = 3f;
		public Rigidbody body;
		public string targetTag = "Player";
		public bool inverse = false;
		public bool rotateOnlyY = true;
        public bool affectYVelocity = false;
		private Transform target;

		protected virtual GameObject GetTargetObject()
		{
			return GameObject.FindGameObjectWithTag(targetTag);
		}

		private void Awake()
		{
			if (!body)
				body = GetComponent<Rigidbody>();

			var targetGameObject = GetTargetObject();
			if (targetGameObject)
				target = targetGameObject.transform;

			if (!sourceTransform)
				sourceTransform = transform;

		}
		private void Update()
		{
			if (!body || !target) return;

			var direction = inverse
				? (sourceTransform.position - target.position).normalized
				: (target.position - sourceTransform.position).normalized;

			var newRotation = Quaternion.LookRotation(direction, Vector3.up);
			if (rotateOnlyY)
			{
				var oldRotation = body.transform.rotation.eulerAngles;
				newRotation = Quaternion.Euler(new Vector3(oldRotation.x, newRotation.eulerAngles.y, oldRotation.z));
			}
			body.transform.rotation = newRotation;

            if (body.isKinematic)
            {
                sourceTransform.Translate(transform.forward * speed * Time.deltaTime, Space.World);
            }
            else
            {
                var newVelocity = transform.forward * speed;
                if (!affectYVelocity)
                {
                    newVelocity = new Vector3(newVelocity.x, body.velocity.y, newVelocity.z);
                }
                body.velocity = newVelocity;                
            }
		}
	}

}