using UnityEngine;
using System.Collections;

namespace Game.Actions
{
	[AddComponentMenu("AI State Actions/ObjectActivatorTimed")]
	public class ObjectActivatorTimed : ObjectActivator
	{
		public float duration = 0.1f;

		protected override void OnEnable()
		{
			base.OnEnable();
			StartCoroutine(Cooldown());
		}

		IEnumerator Cooldown()
		{
			yield return new WaitForSeconds(duration);
			TargetObjectSetActive(false);
		}
	}
}