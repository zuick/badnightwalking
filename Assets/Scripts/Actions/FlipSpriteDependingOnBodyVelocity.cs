using UnityEngine;

namespace Game.Actions
{
	public enum flipBy { x, y, z }

	public class FlipSpriteDependingOnBodyVelocity : MonoBehaviour
	{
		public flipBy dimension;
		public SpriteRenderer spriteRenderer;
		public Rigidbody body;
		public bool inverse;
		public bool onUpdate = false;
		
		void Update()
		{
			if(onUpdate)
				UpdateFlip(body.velocity);
		}

		public void UpdateFlip(Vector3 force)
		{
			if (body && spriteRenderer)
			{
				if (dimension == flipBy.x)
					spriteRenderer.flipX = inverse ? force.x > 0 : force.x <= 0;
				if (dimension == flipBy.y)
					spriteRenderer.flipX = inverse ? force.y > 0 : force.y <= 0;
				if (dimension == flipBy.z)
					spriteRenderer.flipX = inverse ? force.z > 0 : force.z <= 0;
			}
		}
	}
}