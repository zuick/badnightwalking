using UnityEngine;

namespace Game.Actions
{
	[AddComponentMenu("AI State Actions/LookAtMainCameraAdvanced")]
	public class LookAtMainCameraAdvanced : LookAtMainCamera
	{
		public Transform target;

		public bool rotateOnEnable = false;
		public bool rotateOnUpdate = true;

		protected override Transform GetTargetTransform()
		{
			return target;
		}

		void OnEnable()
		{
			if (rotateOnEnable)
				DoRotate();
		}

		protected override void LateUpdate()
		{
			if (rotateOnUpdate)
				DoRotate();
		}
	}
}