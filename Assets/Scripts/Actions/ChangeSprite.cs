using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class ChangeSprite : MonoBehaviour
{
	public SpriteRenderer spriteRenderer;
	public Sprite[] newSprites;
	public Color newColor = Color.white;
	public float transitionTime = 2f;
	public bool restore;

	private Sprite oldSprite;
	private Color oldColor;

	private void OnEnable()
	{
		if (!spriteRenderer) return;

		if (restore)
		{
			oldSprite = spriteRenderer.sprite;
			oldColor = spriteRenderer.color;
		}

		if(newSprites.Length > 0)
			StartCoroutine(DoSpriteTransition());

		StartCoroutine(DoColorTransition());
	}

	IEnumerator DoSpriteTransition()
	{
		var fps = transitionTime / newSprites.Length;
		var currentIndex = 0;
		while(currentIndex < newSprites.Length)
		{
			spriteRenderer.sprite = newSprites[currentIndex];
			currentIndex++;
			yield return new WaitForSeconds(fps);
		}
	}

	IEnumerator DoColorTransition()
	{
		var rd = newColor.r - spriteRenderer.color.r;
		var gd = newColor.g - spriteRenderer.color.g;
		var bd = newColor.b - spriteRenderer.color.b;
		var ad = newColor.a - spriteRenderer.color.a;

		while(spriteRenderer.color != newColor)
		{
			var newColorIteration = new Color(
				spriteRenderer.color.r + rd / transitionTime * Time.deltaTime,
				spriteRenderer.color.g + gd / transitionTime * Time.deltaTime,
				spriteRenderer.color.b + bd / transitionTime * Time.deltaTime,
				spriteRenderer.color.a + ad / transitionTime * Time.deltaTime
			);
			spriteRenderer.color = newColorIteration;
			yield return new WaitForEndOfFrame();

		}
	}

	private void OnDisable()
	{
		if (!spriteRenderer) return;

		if (restore)
		{
			spriteRenderer.sprite = oldSprite;
			spriteRenderer.color = oldColor;
		}
	}
}