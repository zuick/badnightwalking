using UnityEngine;
namespace Game.Actions
{
	[AddComponentMenu("AI State Actions/DestroyObject")]
	public class DestroyObject : MonoBehaviour
	{
		public GameObject target;
		public float delay;

		void OnEnable()
		{
			Destroy(target == null ? gameObject : target, delay);
		}
	}
}