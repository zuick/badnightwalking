using UnityEngine;

namespace Game.Actions
{
	[AddComponentMenu("AI State Actions/InstantiatePrefabWithVelocity")]
	public class InstantiatePrefabWithVelocity : MonoBehaviour
	{
		public Vector2 direction;
		public float speed = 1f;
		public GameObject prefab;

		void OnEnable()
		{
			if (prefab == null) return;

			DoInstance();
		}

		protected virtual GameObject DoInstance()
		{
			var instance = Instantiate(prefab, transform.position, prefab.transform.rotation);
			var body = instance.GetComponentInChildren<Rigidbody2D>();
			if (body)
				body.velocity = direction.normalized * speed;

			return instance;
		}
	}
}