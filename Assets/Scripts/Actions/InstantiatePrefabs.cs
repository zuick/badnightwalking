using UnityEngine;

namespace Game.Actions
{
	[AddComponentMenu("AI State Actions/InstantiatePrefabs")]
	public class InstantiatePrefabs : MonoBehaviour
	{
		public GameObject[] prefabs;

		void OnEnable()
		{
			foreach (var prefab in prefabs)
			{
				if (prefab != null)
					Instantiate(prefab, transform.position, prefab.transform.rotation);
			}
		}
	}
}