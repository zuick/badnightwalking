using UnityEngine;
using UnityEngine.Events;

namespace Game.Actions
{
	[AddComponentMenu("AI State Actions/MoveToNest")]
	public class MoveToNest : MoveToTarget
	{
		public NestHandler nestHandler;

		protected override GameObject GetTargetObject()
		{
			if (nestHandler == null) return null;

			return nestHandler.nestPivot.gameObject;
		}	
	}

}