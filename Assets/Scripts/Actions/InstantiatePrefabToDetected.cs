using UnityEngine;
namespace Game.Actions
{
	[AddComponentMenu("AI State Actions/InstantiatePrefabToDetected")]
	public class InstantiatePrefabToDetected : InstantiatePrefabWithVelocity
	{
		public LayerMask layerMask;
		public CollidersDetector sight;

		protected override GameObject DoInstance()
		{
			if (sight == null)
				return null;

			var target = sight.GetDetected(layerMask);
			if (target == null)
				return null;
			
			direction = target.transform.position - sight.transform.position;
			var instance = base.DoInstance();
			
			var rotation = Quaternion.LookRotation(sight.transform.position - target.transform.position, Vector3.forward);
			rotation.x = 0f;
			rotation.y = 0f;
			instance.transform.rotation = rotation;

			return instance;
		}
	}
}