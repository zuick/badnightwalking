﻿using UnityEngine;
using System.Collections;

namespace Game.Actions
{
    [AddComponentMenu("AI State Actions/DestroyOtherCollider")]
    public class DestroyOtherCollider : MonoBehaviour
    {
        public LayerMask LayerMask;
        public GameObject DestroyEffect;

        void OnCollisionEnter(Collision coll)
        {
            var otherLayer = coll.collider.gameObject.layer;
            if (LayerMask.IsMatch(otherLayer))
            {
                if (DestroyEffect != null) Instantiate(DestroyEffect, coll.collider.transform.position, DestroyEffect.transform.rotation);
                Destroy(coll.collider.gameObject);
            }
        }
    }
}

