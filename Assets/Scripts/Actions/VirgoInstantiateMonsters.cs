using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Game.Actions
{
	public class VirgoInstantiateMonsters : MonoBehaviour
	{
		public enum SpawnType { Around, BeetweenSelfAndPlayer };
		public LayerMask angryHitLayer;
		public GameObject[] prefabs;
		public float maxDistance = 5f;
		public float maxPlayerNearDistance = 1f;
		public float interval = 30;
		public int maxSight = 100;
		public int normalSight = 7;
		public GameObject monsterAppearEffect;
		public GameObject monstersAngryEffect;
		public bool once = true;
		public SpawnType spawnType;
		private List<GameObject> instList = new List<GameObject>();
		private bool evaluated = false;
		private bool angred = false;

		void OnEnable()
		{
			if (evaluated && once) return;

			var player = GameObject.FindGameObjectWithTag("Player");
			if(!player) return;

			foreach (var prefab in prefabs)
			{
				if (prefab != null)
				{
					var center = transform.position;
					if (spawnType == SpawnType.BeetweenSelfAndPlayer)
						center = Vector3.Lerp(transform.position, player.transform.position, 0.5f);
					
					Vector3 pos = new Vector3(
						center.x + UnityEngine.Random.Range(-maxDistance, maxDistance),
						center.y,
						center.z + UnityEngine.Random.Range(-maxDistance, maxDistance)
					);

					if((player.transform.position - pos).magnitude < maxPlayerNearDistance)
						pos -= (player.transform.position - pos).normalized * maxPlayerNearDistance;

					var instance = Instantiate(prefab, pos, prefab.transform.rotation);
					var sight = instance.GetComponentInChildren<CollidersDetector>();
					var hitAgents = instance.GetComponentsInChildren<HitDetectorAgent>();

					if (sight && hitAgents.Length > 0)
					{
						foreach (var a in hitAgents) a.OnHit.AddListener(OnPrefabHit);

						sight.distance = 0f;

						if (monsterAppearEffect)
							Instantiate(monsterAppearEffect, instance.transform.position, monsterAppearEffect.transform.rotation);

						instList.Add(instance);
					}
				}
			}

			evaluated = true;
		}

		public void AngerMonsters()
		{
			if (angred && once) return;

			if (monstersAngryEffect)
				Instantiate(monstersAngryEffect, transform.position, monstersAngryEffect.transform.rotation);

			foreach (var ins in instList)
			{
				var hitAgents = ins.GetComponentsInChildren<HitDetectorAgent>();
				if (hitAgents.Length > 0)
					foreach (var a in hitAgents) a.OnHit.RemoveListener(OnPrefabHit);
			}
			SetSightDistance(maxSight);
			StartCoroutine(ReturnToNormalSight());

			angred = true;
		}

		void OnPrefabHit(Collision other)
		{
			if (angryHitLayer.IsMatch(other.gameObject.layer))
				AngerMonsters();
		}

		void SetSightDistance(int newDistance)
		{
			foreach (var ins in instList)
			{
				var sight = ins.GetComponentInChildren<CollidersDetector>();
				sight.distance = newDistance;
			}
		}

		IEnumerator ReturnToNormalSight()
		{
			yield return new WaitForSeconds(interval);
			SetSightDistance(normalSight);
		}
	}
}