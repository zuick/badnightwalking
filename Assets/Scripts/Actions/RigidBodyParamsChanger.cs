using UnityEngine;
using System;
using System.Collections.Generic;

namespace Game.Actions
{
	[AddComponentMenu("AI State Actions/RigidBodyParamsChanger")]
	public class RigidBodyParamsChanger : MonoBehaviour
	{
		public enum RigidBodyParam { LinearDrag, AngularDrag, Mass, VelocityX, VelocityY }
		[System.Serializable]
		public class Parameter
		{
			public Parameter(RigidBodyParam type, float value)
			{
				this.value = value;
				this.type = type;
			}

			public RigidBodyParam type;
			public float value;
		}

		public Rigidbody rbody;
		public bool restore = true;
		public List<Parameter> newValues;
		List<Parameter> oldValues = new List<Parameter>();

		void OnEnable()
		{
			if (restore)
				SaveOldValues();

			ApplyParameters(newValues);
		}

		void OnDisable()
		{
			if (restore)
			{
				ApplyParameters(oldValues);
				oldValues.Clear();
			}
		}

		void SaveOldValues()
		{
			if (rbody != null && newValues.Count > 0)
			{
				foreach (var c in newValues)
				{
					switch (c.type)
					{
						case RigidBodyParam.AngularDrag:
							oldValues.Add(new Parameter(RigidBodyParam.AngularDrag, rbody.angularDrag));
							break;
						case RigidBodyParam.LinearDrag:
							oldValues.Add(new Parameter(RigidBodyParam.LinearDrag, rbody.drag));
							break;
						case RigidBodyParam.Mass:
							oldValues.Add(new Parameter(RigidBodyParam.Mass, rbody.mass));
							break;
						case RigidBodyParam.VelocityX:
							oldValues.Add(new Parameter(RigidBodyParam.VelocityX, rbody.velocity.x));
							break;
						case RigidBodyParam.VelocityY:
							oldValues.Add(new Parameter(RigidBodyParam.VelocityY, rbody.velocity.y));
							break;
						default: break;
					}
				}
			}
		}

		void ApplyParameters(List<Parameter> changes)
		{
			if (rbody != null && changes.Count > 0)
			{
				foreach (var c in changes)
				{
					switch (c.type)
					{
						case RigidBodyParam.AngularDrag:
							rbody.angularDrag = c.value;
							break;
						case RigidBodyParam.LinearDrag:
							rbody.drag = c.value;
							break;
						case RigidBodyParam.Mass:
							rbody.mass = c.value;
							break;
						case RigidBodyParam.VelocityX:
							rbody.velocity = new Vector2(c.value, rbody.velocity.y);
							break;
						case RigidBodyParam.VelocityY:
							rbody.velocity = new Vector2(rbody.velocity.x, c.value);
							break;
						default: break;
					}
				}
			}
		}
	}
}