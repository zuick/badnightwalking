﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;

namespace Game.Actions
{
	[AddComponentMenu("AI State Actions/PeriodicallyForce")]
	public class PeriodicallyForce : MonoBehaviour
	{
		[System.Serializable]
		public class UnityVector3 : UnityEvent<Vector3> { }

		public bool randomizeX;
		public bool randomizeY;
		public bool randomizeZ;
		public int invertPeriod;
		public bool invertY = false;
		public Vector3 direction;
		public Rigidbody body;
		public float force = 10f;
		public float interval = 3f;
		public float duration = 1f;
		
		public UnityVector3 OnForce = new UnityVector3();

		private MoveToTarget moveToTarget;
		private int invertPeriodCounter = 0;

		void OnEnable()
		{
			if (body == null) return;

			StartCoroutine(DoForcing());
			moveToTarget = GetComponent<MoveToTarget>();
		}

		IEnumerator DoForcing()
		{
			while (enabled)
			{
				yield return new WaitForSeconds(interval);
				if (moveToTarget) moveToTarget.enabled = false;
				var invert = invertPeriod > 0 && invertPeriodCounter % invertPeriod == 0 ? -1f : 1f;

				var newDirection = new Vector3(
					invert * (randomizeX ? direction.x * (UnityEngine.Random.Range(0f, 1f) > 0.5f ? -1f : 1f) : direction.x),
					(invertY ? invert : 1f) * (randomizeY ? direction.y * (UnityEngine.Random.Range(0f, 1f) > 0.5f ? -1f : 1f) : direction.y),
					invert * (randomizeZ ? direction.z * (UnityEngine.Random.Range(0f, 1f) > 0.5f ? -1f : 1f) : direction.z)
				);

				var newForce = newDirection * force;
				if(OnForce != null)
					OnForce.Invoke(newForce);

				body.AddRelativeForce(newForce, ForceMode.Impulse);
				if (invertPeriod > 0)
					invertPeriodCounter++;

				yield return new WaitForSeconds(duration);
				if (moveToTarget) moveToTarget.enabled = true;
			}
		}
	}
}

