using UnityEngine;
using UnityEngine.Events;

namespace Game.Actions
{
	[AddComponentMenu("AI State Actions/MoveForwardAndTurn")]
	public class MoveForwardAndTurn : MonoBehaviour
	{
		public Transform sourceTransform;
		public float speed = 3f;
		public Vector3 angularSpeed;
		public Rigidbody body;
		public bool resetXRotation = true;

		private void Awake()
		{
			if (!body)
				body = GetComponent<Rigidbody>();

			if (!sourceTransform)
				sourceTransform = transform;
		}

		private void OnEnable() {
			if(resetXRotation)
			{
				var oldRotation = sourceTransform.rotation.eulerAngles;
				var newRotation = Quaternion.Euler(new Vector3(
					0f, oldRotation.y, oldRotation.z
				));
				
				sourceTransform.rotation = newRotation;
			}
		}
		private void Update()
		{
			if (!body) return;

			var oldRotation = sourceTransform.rotation.eulerAngles;

			var newDeltaRotation = new Vector3(
				angularSpeed.x * Time.deltaTime,
				angularSpeed.y * Time.deltaTime,
				angularSpeed.z * Time.deltaTime
			);

			var newRotation = Quaternion.Euler(new Vector3(
				oldRotation.x + newDeltaRotation.x,
				oldRotation.y + newDeltaRotation.y,
				oldRotation.z + newDeltaRotation.z
			));

			sourceTransform.rotation = newRotation;

			if (body.isKinematic)
				sourceTransform.Translate(sourceTransform.forward * speed * Time.deltaTime, Space.World);
			else
				body.velocity = sourceTransform.forward * speed;
		}
	}

}