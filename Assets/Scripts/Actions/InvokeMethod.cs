using UnityEngine;
using UnityEngine.Events;

namespace Game.Actions
{
	[AddComponentMenu("AI State Actions/InvokeMethod")]
	public class InvokeMethod : MonoBehaviour
	{
		public float delay = 0f;
		public UnityEvent method;

		private void OnEnable()
		{
			Invoke("DoInvoke", delay);
		}

		private void DoInvoke()
		{
			if (method != null)
				method.Invoke();
		}
	}
}