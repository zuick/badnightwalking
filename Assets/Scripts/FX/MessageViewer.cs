using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using UnityEngine.Events;

[RequireComponent(typeof(Text))]
public class MessageViewer : MonoBehaviour
{
	public UnityEvent OnCompleted = new UnityEvent();
	public string[] phrases;
	public float letterPerSecond;

	private Text target;
	private void OnEnable()
	{
		target = GetComponent<Text>();
		target.text = "";
		StartCoroutine(ProcessLetters());
	}

	private void OnDisable()
	{
		StopAllCoroutines();
	}

	IEnumerator ProcessLetters()
	{
		var currentPhraseIndex = 0;
		while (currentPhraseIndex < phrases.Length)
		{
			var letterIndex = 0;
			target.text = "";
			while (letterIndex < phrases[currentPhraseIndex].Length)
			{
				target.text += phrases[currentPhraseIndex][letterIndex];
				yield return new WaitForSeconds(1f / letterPerSecond);
				letterIndex++;
			}
			yield return new WaitUntil(() => { return Input.anyKeyDown; });
			currentPhraseIndex++;
		}

		if(OnCompleted != null)
			OnCompleted.Invoke();

		yield return null;
	}
}