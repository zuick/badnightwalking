using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class RandomizeSound : MonoBehaviour
{
	public float randomizePitching = 0.05f;
	public float randomizeVolume = 0.05f;

	private void Awake()
	{
		var source = GetComponent<AudioSource>();
		source.pitch += UnityEngine.Random.Range(-randomizePitching, randomizePitching);
		source.volume += UnityEngine.Random.Range(-randomizeVolume, randomizeVolume);
	}
}