using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class IntervalAudioClip : MonoBehaviour {
	public AudioClip[] clips;
	public float minDelay;
	public float maxDelay;
	private AudioSource source;

	private void Awake() {
		source = GetComponent<AudioSource>();
	}

	private void Start() {
		if(clips.Length > 0)
			StartCoroutine(DoSounds());
	}

	IEnumerator DoSounds()
	{
		while(enabled)
		{
			yield return new WaitForSeconds(UnityEngine.Random.Range(minDelay, maxDelay));
			source.clip = clips[UnityEngine.Random.Range(0, clips.Length)];
			source.Play();
		}
	}
}