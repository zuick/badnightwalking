using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class VisibilityDistance : MonoBehaviour
{
	public float minDistance = 10f;
	public float maxDistance = 20f;

	private SpriteRenderer spriteRenderer;
	private Camera mainCamera;

	private void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		mainCamera = Camera.main;
	}

	private void Update()
	{
		if (mainCamera && spriteRenderer)
		{
			var distance = (transform.position - mainCamera.transform.position).magnitude;
			if (distance > maxDistance)
				spriteRenderer.color = ChangeColorAlphaTo(spriteRenderer.color, 0f);
			else if (distance < minDistance)
				spriteRenderer.color = ChangeColorAlphaTo(spriteRenderer.color, 1f);
			else if (distance < maxDistance && distance > minDistance)
				spriteRenderer.color = ChangeColorAlphaTo(
					spriteRenderer.color,
					1f - (distance - minDistance) / (maxDistance - minDistance) 
				);
		}
	}

	private Color ChangeColorAlphaTo(Color c, float a)
	{
		var newColor = new Color(c.r, c.g, c.b, a);
		return newColor;
	}
}