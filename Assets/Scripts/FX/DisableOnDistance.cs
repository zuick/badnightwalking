using UnityEngine;

public class DisableOnDistance : MonoBehaviour
{
	public string targetTag = "Player";
	public float maxDistance = 50;
	public GameObject objectToHide;
	private Transform targetTransform;

	private void Awake()
	{
		var player = GameObject.FindGameObjectWithTag(targetTag);
		if (player)
			targetTransform = player.transform;
	}

	private void FixedUpdate()
	{
		if (targetTransform == null) return;
		var distance = (targetTransform.position - transform.position).magnitude;

		objectToHide.SetActive(distance <= maxDistance);
	}
}