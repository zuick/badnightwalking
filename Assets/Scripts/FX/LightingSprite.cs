using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class LightingSprite : MonoBehaviour
{
	private SpriteRenderer parentSprite;
	private SpriteRenderer lightingSprite;

	private void Awake()
	{
		lightingSprite = GetComponent<SpriteRenderer>();
		parentSprite = transform.parent.GetComponent<SpriteRenderer>();
	}

	private void LateUpdate()
	{
		if (!parentSprite || !lightingSprite) return;

		lightingSprite.flipX = parentSprite.flipX;
		lightingSprite.flipY = parentSprite.flipY;
		if (lightingSprite.sprite != null && parentSprite.sprite != null && lightingSprite.sprite == parentSprite.sprite) return;

		lightingSprite.sprite = parentSprite.sprite;
		//lightingSprite.color = ChangeColorAlphaTo(lightingSprite.color, 1f - AverageColor(parentSprite.color));
	}

	private float AverageColor(Color c)
	{
		return (c.r + c.g + c.b) / 3f;
	}

	private Color ChangeColorAlphaTo(Color c, float a)
	{
		var newColor = new Color(c.r, c.g, c.b, a);
		return newColor;
	}
}