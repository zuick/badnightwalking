using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class RandomSpriteFlipper : MonoBehaviour {
	void Awake(){
		GetComponent<SpriteRenderer>().flipX = Random.Range(0f,1f) > 0.5f;
	}
}