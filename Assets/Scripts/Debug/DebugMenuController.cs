﻿using UnityEngine;
using System.Collections;

public class DebugMenuController : MonoBehaviour
{
	void Awake()
	{
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}

	void Update()
	{
		if (Input.GetButtonDown("Cancel"))
			Application.Quit();
	}
}

