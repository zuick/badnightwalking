using UnityEngine;

public static class LayerExtension
{
	public static bool IsMatch(this LayerMask mask, int layer)
	{
		return mask.value == (mask.value | (1 << layer));
	}
}