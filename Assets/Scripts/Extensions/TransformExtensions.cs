using UnityEngine;

public static class TransformExtensions
{
	public static Transform Clear(this Transform trans)
	{
		var total = trans.childCount;
		for (var i = 0; i < total; i++)
		{
			var child = trans.GetChild(i);
			if (child != null)
				GameObject.Destroy(child.gameObject);
		}
		return trans;
	}
}