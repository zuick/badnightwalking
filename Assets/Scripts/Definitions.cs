﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class GameObjectEvent : UnityEvent<GameObject> { }

[System.Serializable]
public class UnityCollisionEvent : UnityEvent<Collision> { }

[System.Serializable]
public class UnityColliderEvent : UnityEvent<Collider> { }

public class UnityEventString : UnityEvent<string> { }

public class UnityEventInt : UnityEvent<int> { }