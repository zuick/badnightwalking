using UnityEngine;
using System.Collections;
using System;

public class MainMenuController : MonoBehaviour
{
	public Transform targetCamera;
	public ShootingController shooting;
	public FPSController moving;

	public float rotateX = 45f;
	public float rotateDuration = 3f;

	private void Start()
	{
		if (!targetCamera || !shooting || !moving) return;

		targetCamera.localRotation = Quaternion.Euler(rotateX, 0f, 0f);

		if (WindowsManager.Instance == null) return;
		var mainMenu = WindowsManager.Instance.GetWindow("MainMenuWindow");

		shooting.enabled = false;
		moving.enabled = false;

		mainMenu.OnClose(() =>
		{
			StartCoroutine(RotateCamera());
		});

		mainMenu.OnHidden(() =>
		{
			shooting.enabled = true;
			moving.enabled = true;
		});
	}

	IEnumerator RotateCamera()
	{
		var rotatePerSec = (rotateX / rotateDuration);
		var elapsedTime = 0f;
		while (elapsedTime < rotateDuration)
		{
			targetCamera.localRotation = Quaternion.Euler(
				targetCamera.localRotation.eulerAngles.x - rotatePerSec * Time.deltaTime, 0f, 0f);

			elapsedTime += Time.deltaTime;
			yield return null;
		}

		targetCamera.localRotation = Quaternion.Euler(0f, 0f, 0f);
	}
}