using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : Singleton<LevelController>
{
	private Window confirmExit;

	private void Start()
	{
		HideCursor();
	}

	public void ReloadScene()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	void Update()
	{
		if (Input.GetButtonDown("Cancel") && confirmExit == null)
		{
			confirmExit = WindowsManager.Instance.GetWindow();
			if (confirmExit == null) return;

			confirmExit.Build("Выйти из игры?");

			confirmExit.OnHidden(() =>
			{
				confirmExit = null;
			});

			confirmExit.OnSubmit(() =>
			{

				if (Fader.instance)
				{
					Fader.instance.FadeIn();
					Fader.instance.OnFadeIn += ToMainMenu;
				}
				else
					ToMainMenu();

				confirmExit.Close();
			});
		}
	}

	void ToMainMenu()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
	}

	private void HideCursor()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}
}