using UnityEngine;

public class WindowsCachedResources : CachedResources
{
	public static WindowsCachedResources Instance;
	protected override string PrefabsPath { get { return "UI/Windows"; } }
	protected override void Awake(){
		base.Awake();
		Instance = this;
	}
}