using UnityEngine;
using UnityEngine.SceneManagement;

public class GoodEndingController : MonoBehaviour
{
	public string lastScene;

	private void Start()
	{
		if (Fader.instance)
			Fader.instance.FadeOut();

		HideCursor();
	}

	public void End()
	{
		if (Fader.instance)
		{
			Fader.instance.FadeIn();
			Fader.instance.OnFadeOut += () =>
			{
				HideCursor();
				SceneManager.LoadScene(lastScene);
			};
		}

	}

	private void HideCursor()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}
}