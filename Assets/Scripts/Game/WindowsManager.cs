using UnityEngine;
using System.Collections.Generic;

public class WindowsManager : Singleton<WindowsManager>
{
	public Transform WindowsPivot;
	private List<Window> activeWindows = new List<Window>();

	public int Count
	{
		get
		{
			return activeWindows.Count;
		}
	}

	public Window GetWindow(string windowPrefab = "ConfirmWindow")
	{
		if(WindowsCachedResources.Instance == null) return null;
		
		var prefab = WindowsCachedResources.Instance.GetPrefab(windowPrefab);
		if (prefab == null)
		{
			Debug.LogError("Can't find prefab: " + windowPrefab);
			return null;
		}
		var parent = (GameObject)Instantiate(prefab, WindowsPivot, false);
		var window = parent.GetComponentInChildren<Window>();
		return window;
	}

	public void Add(Window w)
	{
		activeWindows.Add(w);
	}

	public void Remove(Window w)
	{
		activeWindows.Remove(w);
		GameObject.Destroy(w.gameObject);
	}

	public bool IsWindowActive(Window w)
	{
		return activeWindows[activeWindows.Count - 1] == w;
	}

	public bool IsWindowActive(string prefabName)
	{
		return activeWindows[activeWindows.Count - 1].gameObject.name == prefabName;
	}
}