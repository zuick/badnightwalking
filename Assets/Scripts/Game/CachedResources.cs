﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public class CachedResources : MonoBehaviour
{
	public GameObject[] Prefabs;
	protected virtual string PrefabsPath { get { return ""; } }

	protected virtual void Awake()
	{
		Prefabs = Resources.LoadAll<GameObject>(PrefabsPath);
	}

	public GameObject GetPrefab(string name)
	{
		return Prefabs.FirstOrDefault(p => p.name.ToLower() == name.ToLower());
	}
}
