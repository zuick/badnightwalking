using UnityEngine;

public class SmartShadow : MonoBehaviour
{
	public LayerMask groundLayer;
	public float yDelta = -0.1f;
	public Transform raycastPivot;

	void Update()
	{
		var hits = Physics.RaycastAll(new Ray(raycastPivot.position, -raycastPivot.up), 100f, groundLayer);
		if(hits.Length > 0)
		{
			var hitPoint = hits[0].point;
			transform.position = new Vector3(hitPoint.x, hitPoint.y + yDelta, hitPoint.z);
		}
	}
}