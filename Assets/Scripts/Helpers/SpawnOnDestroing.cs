using UnityEngine;

public class SpawnOnDestroing : MonoBehaviour {
	public GameObject prefab;
	public Transform pivot;

	private void OnDestroing() {
		if(prefab != null){
			var instance = Instantiate(prefab);
			instance.transform.position = pivot != null ? pivot.position : transform.position;
		}
	}
}