using UnityEngine;

public class EnableFog : MonoBehaviour {
	public bool fog;

	private void Awake() {
		RenderSettings.fog = fog;
	}
}