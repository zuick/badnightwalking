﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class LookAtMainCamera : MonoBehaviour
{
	public bool x = true;
	public bool y = true;
	public bool z = true;

	protected Transform CameraTransform;

	protected virtual void Awake()
	{
		if (Camera.main != null)
		{
			CameraTransform = Camera.main.transform;
		}
	}

	protected virtual Transform GetTargetTransform()
	{
		return transform;
	}

	protected virtual void LateUpdate()
	{
		DoRotate();
	}

	protected virtual void DoRotate()
	{
		if (CameraTransform != null)
		{
			var targetTransform = GetTargetTransform();
			if(targetTransform == null) return;
			
			var newRotation = Quaternion.LookRotation(-CameraTransform.forward, CameraTransform.up).eulerAngles;
			var filtered = new Vector3(
				x ? newRotation.x : targetTransform.eulerAngles.x,
				y ? newRotation.y : targetTransform.eulerAngles.y,
				x ? newRotation.z : targetTransform.eulerAngles.z
			);
			targetTransform.eulerAngles = filtered;
		}
	}
}