using UnityEngine;

public class AutoMove : MonoBehaviour {
	public Vector3 delta;
	public float maxDistance = 2;
	public bool cicle = true;
	public Transform target;

	Vector3 startPosition;
	float dir = 1f;
	private void Awake() {
		startPosition = transform.localPosition;
		if (!target)
		{
			target = transform;
		} 
			
	}

	private void Update() {
		target.localPosition = new Vector3(
			target.localPosition.x + delta.x * Time.deltaTime * dir,
			target.localPosition.y + delta.y * Time.deltaTime * dir,
			target.localPosition.z + delta.z * Time.deltaTime * dir
		);
		if(((target.localPosition - startPosition).magnitude >= maxDistance) && (cicle)){
			dir *= -1f;
			startPosition = target.localPosition;
		}
	} 
}