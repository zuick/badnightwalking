using UnityEngine;

public class ScaleObject : MonoBehaviour
{
	public Transform target;
	public Vector3 newScale;

	void OnEnable()
	{
		if(target != null)
			target.localScale = newScale;
	}
}