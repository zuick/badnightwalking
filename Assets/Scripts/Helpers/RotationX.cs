﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationX : MonoBehaviour {

	public int angleX = 0;
	// Use this for initialization
	void Start () {
		transform.rotation = Quaternion.Euler(angleX, transform.rotation.y, transform.rotation.z);
	}

}
