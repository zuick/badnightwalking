using UnityEngine;
using System.Linq;

public class NestHandler : MonoBehaviour {
	public Transform nestPivot;
	public bool AtNest
	{
		get { return transform.IsChildOf(nestPivot); }
	}
	
	private void Awake() {
		if(!enabled) return;
		var nest = new GameObject();
		nest.name = "Nest";
		nest.transform.SetParent(transform.parent);
		nest.transform.localPosition = transform.localPosition;
		transform.SetParent(nest.transform);
		transform.localPosition = Vector3.zero;
		nestPivot = nest.transform;
	}

	private void Start() {
		var ownRenderer = GetComponentInChildren<SpriteRenderer>();
		var otherRenderer = nestPivot.parent.GetComponentsInChildren<SpriteRenderer>()
			.FirstOrDefault(r => r != ownRenderer);

		if(otherRenderer == null) return;

		if(otherRenderer.flipX)
			nestPivot.transform.localPosition = new Vector3(
				-nestPivot.transform.localPosition.x,
				nestPivot.transform.localPosition.y,
				nestPivot.transform.localPosition.z
			);
	}

	public void ToNest()
	{
		if(nestPivot != null)
		{
			transform.SetParent(nestPivot);
			transform.localPosition = Vector3.zero;
		}
	}

	public void FromNest()
	{
		transform.SetParent(null);
	}
}