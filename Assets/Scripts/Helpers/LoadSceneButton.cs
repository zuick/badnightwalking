﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class LoadSceneButton : MonoBehaviour
{
    public string target;

    void Awake()
    {
        if (target == null) return;

        GetComponent<Button>().onClick.AddListener(() =>
            {
                SceneManager.LoadScene(target);
            }
        );

        var text = GetComponentInChildren<Text>();
        if (text) text.text = target;
    }
}

