﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class SetLightIntencity : MonoBehaviour {
	public float intencity = 0.2f;
	private void Awake() {
		var light = GetComponent<Light>();
		light.intensity = intencity;
	}
}
