using UnityEngine;

public class MovingWavering : MonoBehaviour
{
	public enum MovingAxis
	{
		x, y, z
	}

	public MovingAxis axis = MovingAxis.x;
	public float amplitude = 1f;
	public float period = 1f;

	private Vector3 oldPosition;
	private float counter;

	private void Awake()
	{
		oldPosition = transform.localPosition;
	}

	private void Update()
	{
		counter += Time.deltaTime / period;
		var delta = amplitude * Mathf.Sin(counter);

		var newPosition = oldPosition;
		if (axis == MovingAxis.x)
			newPosition.x += delta;
		if (axis == MovingAxis.y)
			newPosition.y += delta;
		if (axis == MovingAxis.z)
			newPosition.z += delta;

		transform.localPosition = newPosition;
	}
}