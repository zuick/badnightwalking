using UnityEngine;
using System.Collections;

public class HitEffectSpawner : Sounding
{
	public LayerMask layerMask;
	public GameObject effectPrefab;
	public HitDetectorAgent hitDetectorAgent;

	protected override void Awake()
	{
		base.Awake();
		if (hitDetectorAgent == null)
			hitDetectorAgent = GetComponent<HitDetectorAgent>();

		if (hitDetectorAgent != null)
			hitDetectorAgent.OnHit.AddListener(OnCollision);
	}
	private void OnDestroy()
	{
		if (hitDetectorAgent != null)
			hitDetectorAgent.OnHit.AddListener(OnCollision);
	}

	void OnCollision(Collision coll)
	{
		if (layerMask.IsMatch(coll.collider.gameObject.layer) && coll.contacts.Length > 0)
		{
			var instance = Instantiate(effectPrefab);
			instance.transform.position = coll.contacts[0].point;
			DoSound();
		}
	}
}