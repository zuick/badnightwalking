using UnityEngine;

namespace Game.AI
{
	public class GameComponentsProvider : ComponentsProvider
	{
		public HitDetector hitDetector;
		public CollidersDetector sight;

		public override void SetComponentsEnabled(bool isEnabled)
		{
			if (hitDetector != null)
				hitDetector.enabled = isEnabled;
			if (sight != null)
				sight.enabled = isEnabled;
		}
	}
}