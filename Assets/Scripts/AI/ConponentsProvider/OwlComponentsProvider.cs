using UnityEngine;

namespace Game.AI
{
	public class OwlComponentsProvider : GameComponentsProvider
	{
		public NestHandler nestHandler;

		public override void SetComponentsEnabled(bool isEnabled)
		{
			base.SetComponentsEnabled(enabled);

			if(nestHandler)
				nestHandler.enabled = isEnabled;
		}
	}
}