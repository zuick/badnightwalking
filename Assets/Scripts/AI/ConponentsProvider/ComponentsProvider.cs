using UnityEngine;

namespace Game.AI
{
	public class ComponentsProvider : MonoBehaviour
	{
		public virtual void SetComponentsEnabled(bool isEnabled) { }
	}
}