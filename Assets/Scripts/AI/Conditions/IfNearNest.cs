using UnityEngine;

namespace Game.AI
{
	[CreateAssetMenu(fileName = "IfNearNest", menuName = "AI/IfNearNest", order = 0)]
	public class IfNearNest : Condition
	{
		[Range(0, 1)]
		public float maxDistance = 0.1f;

		public override bool Check(StateMachine sm)
		{
			var nestHandler = (sm.components as OwlComponentsProvider).nestHandler;
			if(nestHandler == null) return false;

			return (sm.transform.position - nestHandler.nestPivot.transform.position).magnitude <= maxDistance;
		}
	}
}