using UnityEngine;
using System.Linq;

namespace Game.AI
{
	[CreateAssetMenu(fileName = "IfHitDetected", menuName = "AI/IfHitDetected", order = 0)]
	public class IfHitDetected : Condition
	{
		public LayerMask layerMask;

		public override bool Check(StateMachine sm)
		{
			var hitDetector = (sm.components as GameComponentsProvider).hitDetector;
			if (hitDetector == null)
				return false;

			return hitDetector.collisions.Keys.Any(l => layerMask.IsMatch(l));
		}
	}
}