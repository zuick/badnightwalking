using UnityEngine;
using System.Linq;

namespace Game.AI
{
	[CreateAssetMenu(fileName = "IfColliderDetected", menuName = "AI/IfColliderDetected", order = 0)]
	public class IfColliderDetected : Condition
	{
		public LayerMask layerMask;

		public override bool Check(StateMachine sm)
		{
			var sight = (sm.components as GameComponentsProvider).sight;
			if (sight == null || sight.visibleColliders.Count == 0)
				return false;

			return sight.GetDetected(layerMask) != null;
		}
	}
}