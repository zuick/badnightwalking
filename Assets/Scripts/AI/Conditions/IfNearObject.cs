using UnityEngine;

namespace Game.AI
{
	[CreateAssetMenu(fileName = "IfNearObject", menuName = "AI/IfNearObject", order = 0)]
	public class IfNearObject : Condition
	{
		public LayerMask raycastLayer;
		[Range(0, 100)]
		public float maxDistance = 0.5f;

		public override bool Check(StateMachine sm)
		{
			var otherColliders = Physics.OverlapSphere(sm.transform.position, maxDistance, raycastLayer);
			return otherColliders.Length > 0;
		}
	}
}