using UnityEngine;
using UnityEngine.Events;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Game.AI
{
	public class StateMachine : MonoBehaviour
	{
		[Serializable]
		public class Transition
		{
			public GameObject state;
			public GameObject nextState;
			public bool active = true;
		}
		[Serializable]
		public class ConditionalTransition : Transition
		{
			public Condition condition;
			public bool inverseCondition = false;
			public float delay;
		}

		[Serializable]
		public class AutoTransition : Transition
		{
			public float time;
		}

		public UnityEventString OnStateChanged = new UnityEventString();

		public ConditionalTransition[] conditionalTransitions;
		public AutoTransition[] autoTransitions;
		public Transform statesPivot;

		public ComponentsProvider components;
		public bool disableProvidedComponentsIfNotActive = false;

		private bool initialed;
		private GameObject currentState;

		void Start()
		{
			DoFirstTransition();
			initialed = true;
		}


		void OnEnable()
		{
			if (statesPivot == null || statesPivot.childCount == 0)
				return;
			
			for (var i = 0; i < statesPivot.childCount; i++)
				statesPivot.GetChild(i).gameObject.SetActive(false);

			if (disableProvidedComponentsIfNotActive)
				components.SetComponentsEnabled(true);

			if (initialed)
				DoFirstTransition();
		}

		void DoFirstTransition()
		{
			if (statesPivot == null || statesPivot.childCount == 0)
				return;

			if(currentState == null)
				currentState = statesPivot.GetChild(0).gameObject;

			DoTransition(null, currentState);
		}

		void Update()
		{
			if (currentState == null)
				return;

			var newTransition = conditionalTransitions.FirstOrDefault(
				t => t.active &&
					(t.state == currentState || t.state == null) && //  current state or any
					t.condition != null && t.condition.Check(this) ^ t.inverseCondition &&
					t.nextState != null
			);

			if (newTransition != null)
			{
				if (newTransition.delay == 0f)
					DoTransition(currentState, newTransition.nextState);
				else
					StartCoroutine(DoTransition(currentState, newTransition.nextState, newTransition.delay));
			}

		}

		void DoTransition(GameObject oldState, GameObject newState)
		{
			if (oldState == newState)
				return;

			StopAllCoroutines();

			if (oldState != null)
				oldState.SetActive(false);

			if (newState != null)
			{
				newState.SetActive(true);
				currentState = newState;
				OnStateChanged.Invoke(newState.name);
				var autoTransition = autoTransitions.FirstOrDefault(t => t.active && t.state == newState);
				if (autoTransition != null)
				{
					if (autoTransition.time > 0)
						StartCoroutine(DoTransition(autoTransition.state, autoTransition.nextState, autoTransition.time));
					else
						DoTransition(autoTransition.state, autoTransition.nextState);
				}
			}
		}

		IEnumerator DoTransition(GameObject oldState, GameObject newState, float time)
		{
			yield return new WaitForSeconds(time);
			if (currentState == oldState)
				DoTransition(oldState, newState);
		}

		public void SetStateFromString(string stateName)
		{
			var state = transform.Find(stateName);
			var stateInLowerCase = transform.Find(stateName.ToLower());
			if (state != null)
			{
				currentState = state.gameObject;
			}
			else if (stateInLowerCase != null)
			{
				currentState = stateInLowerCase.gameObject;
			}
		}

		public void Deactivate()
		{
			if (disableProvidedComponentsIfNotActive)
				components.SetComponentsEnabled(false);

			enabled = false;
		}
	}
}