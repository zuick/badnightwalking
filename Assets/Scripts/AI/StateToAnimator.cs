using UnityEngine;
using System.Linq;
using Elendow.SpritedowAnimator;
namespace Game.AI
{
	public class StateToAnimator : MonoBehaviour
	{
		[System.Serializable]
		public class StateMapping
		{
			public GameObject from;
			public SpriteAnimation to;
			public bool backward;
			public bool loop = true;
		}

		public SpriteAnimator animator;
		public StateMapping[] Map;
		
		private StateMachine[] stateMachines;
		void Awake()
		{
			stateMachines = GetComponentsInChildren<StateMachine>();
			if (stateMachines.Length == 0)
				return;

			foreach(var sm in stateMachines)
				sm.OnStateChanged.AddListener(OnStateChanged);
		}

		void OnStateChanged(string stateName)
		{
			if (animator == null)
				return;

			var stateMapping = Map.FirstOrDefault(s => s.from.name == stateName || s.from.name == stateName.ToLower());
			if (stateMapping != null)
				animator.Play(stateMapping.to.Name, !stateMapping.loop, stateMapping.backward);
		}

		private void OnDestroy()
		{
			if (stateMachines.Length == 0)
				return;

			foreach(var sm in stateMachines)
				sm.OnStateChanged.RemoveListener(OnStateChanged);
		}
	}
}