using UnityEngine;

public class Sounding : MonoBehaviour
{
	AudioSource audioSource;

	protected virtual void Awake()
	{
		audioSource = GetComponent<AudioSource>();
	}

	public void DoSound()
	{
		if (audioSource != null)
			audioSource.Play();
	}

}