using UnityEngine;

public class SoundEffect : Sounding {
    protected override void Awake () {
        base.Awake ();
        DoSound ();
    }
}