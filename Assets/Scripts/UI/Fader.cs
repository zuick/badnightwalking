﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
	public CanvasGroup canvas;
	public float fadeTime;
	public Text caption;
	public bool fadeOutOnAwake = false;

	[HideInInspector]
	public static Fader instance;

	public System.Action OnFadeIn;
	public System.Action OnFadeOut;
	public bool isFading { get { return _fading; } }
	private bool _fading = false;

	void Awake(){
		instance = this;	
		if(fadeOutOnAwake){
			canvas.alpha = 1f;
			FadeOut();
		}
	}

	public void SetCaption(string text)
	{
		if(caption == null)
			return;
		
		caption.text = text;
	}

	public void FadeIn(string caption = ""){
		SetCaption(caption);
		if(!_fading){
			_fading = true;
			StartCoroutine(Fading(1f));
		}
	}

	public void FadeOut(){
		if(!_fading){
			_fading = true;
			StartCoroutine(Fading(0f));
		}
	}

	IEnumerator Fading(float endAlpha){
		var startAlpha = canvas.alpha;

		while(canvas.alpha != endAlpha){			
			canvas.alpha += (endAlpha - startAlpha) * Time.deltaTime * 1f / fadeTime;
			yield return new WaitForEndOfFrame();
		}
		_fading = false;
		if(endAlpha >= startAlpha && OnFadeIn != null){
			OnFadeIn();
		}else if(OnFadeOut != null){
			OnFadeOut();
		}
		OnFadeIn = null;
		OnFadeOut = null;
	}
}

