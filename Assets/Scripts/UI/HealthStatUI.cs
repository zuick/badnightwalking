using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthStatUI : MonoBehaviour
{
	public string format = "Heakth: {0}";
	private Text text;
	private PlayerHealth playerHealth;
	private bool hidden = true;

	void Awake()
	{
		text = GetComponent<Text>();
		var player = GameObject.FindGameObjectWithTag("Player");
		if (player) playerHealth = player.GetComponentInChildren<PlayerHealth>();
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.H))
			hidden = !hidden;

		if (!text || !playerHealth) return;

		text.text = hidden ? "" : string.Format(format, playerHealth.Value);
	}
}

