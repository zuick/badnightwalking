using UnityEngine;
using UnityEngine.Events;

public class MessageHolder: MonoBehaviour
{
	public UnityEvent OnRead = new UnityEvent();

	[TextArea(5,20)]
	[SerializeField]
	private string message;

	public string Read()
	{
		if(OnRead != null) OnRead.Invoke();
		
		return message;
	}
}