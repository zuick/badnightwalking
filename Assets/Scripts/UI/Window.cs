﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[DisallowMultipleComponent]
[RequireComponent(typeof(Animator))]
public class Window : MonoBehaviour
{

	protected Animator anim = null;
	[SerializeField]
	protected Text text;
	[SerializeField]
	protected Button submitButton;
	[SerializeField]
	protected Button closeButton;
	[SerializeField]
	protected bool cancelable;

	protected float removeDelay = 1f;

	private System.Action onClose;
	private System.Action onSubmit;
	private System.Action onShown;
	private System.Action onHidden;
	protected bool _closing = false;

	protected virtual void Start()
	{
		anim = GetComponent<Animator>();
		if (closeButton != null) closeButton.onClick.AddListener(Close);
		if (submitButton != null) submitButton.onClick.AddListener(Submit);
		if (WindowsManager.Instance)
			WindowsManager.Instance.Add(this);
	}

	public virtual bool IsActive
	{
		get
		{
			if (!WindowsManager.Instance)
				return false;

			return WindowsManager.Instance.IsWindowActive(this);
		}
	}

	public virtual void Build(string description)
	{
		text.text = description;
	}

	public virtual void OnClose(System.Action action)
	{
		onClose += action;
	}

	public virtual void OnSubmit(System.Action action)
	{
		onSubmit += action;
	}

	public virtual void OnShown(System.Action action)
	{
		onShown += action;
	}

	public virtual void OnHidden(System.Action action)
	{
		onHidden += action;
	}

	public virtual void Close()
	{
		if (onClose != null)
			onClose();

		anim.SetTrigger("Hide");
		_closing = true;
	}

	public virtual void Submit()
	{
		if (onSubmit != null)
			onSubmit();
	}

	public virtual void Shown()
	{
		if (_closing) return;

		if (onShown != null)
			onShown();
	}

	public virtual void Hidden()
	{
		if (onHidden != null)
			onHidden();

		if (WindowsManager.Instance)
			WindowsManager.Instance.Remove(this);
	}

	protected virtual void Activate()
	{
		anim.SetTrigger("Show");
	}

	private IEnumerator RemoveWithDelay()
	{
		yield return new WaitForSeconds(removeDelay);
		if (WindowsManager.Instance)
			WindowsManager.Instance.Remove(this);
	}

	protected virtual bool isMainButtonsAllowed()
	{
		return IsActive;
	}

	protected virtual void Update()
	{
		if (_closing)
			return;

		if (isMainButtonsAllowed())
		{
			if (Input.GetButtonDown("Cancel") && cancelable)
			{
				if (closeButton != null)
				{
					Close();
				}
				else
				{
					Submit();
				}
			}
			else if (Input.GetButtonDown("Submit"))
			{
				Submit();
			}
		}
	}
}

