﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShootingStatUI : MonoBehaviour
{
    public string format = "Lights: {0}";
    private Text text;
    private ShootingController shootingController;

    void Awake()
    {
        text = GetComponent<Text>();
        var player = GameObject.FindGameObjectWithTag("Player");
        if (player) shootingController = player.GetComponentInChildren<ShootingController>();
    }

    void Update()
    {
        if (!text || !shootingController) return;

        text.text = shootingController.Bullets == 0 ? "" : string.Format(format, shootingController.Bullets);
    }
}

