using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PausingWindow : Window
{
	public bool handleCursor = true;
	public bool pause = true;

	public override void Close()
	{
		base.Close();
		if (handleCursor)
			HideCursor();

		if (pause)
			Time.timeScale = 1;
	}

	public override void Shown()
	{
		base.Shown();
		if (handleCursor)
			ShowCursor();
		if (pause)
			Time.timeScale = 0;
	}

	private void HideCursor()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	private void ShowCursor()
	{
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}
}