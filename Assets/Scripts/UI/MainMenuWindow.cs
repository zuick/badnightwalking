using UnityEngine;

public class MainMenuWindow : PausingWindow
{
	protected override void Update()
	{
		if (_closing)
			return;

		if (isMainButtonsAllowed() && Input.anyKeyDown && !Input.GetButtonDown("Cancel"))
		{
			Close();
		}
	}
}