﻿using UnityEngine;

/// <summary>
/// Be aware this will not prevent a non singleton constructor
///   such as `T myT = new T();`
/// To prevent that, add `protected T () {}` to your singleton class.
/// 
/// As a note, this is made as MonoBehaviour because we need Coroutines.
/// </summary>
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	public bool dontDestroyOnLoad = true;
	protected virtual void Awake()
	{
		if (_instance == null)
		{
			_instance = FindObjectOfType<T>();
			if (dontDestroyOnLoad)
			{
				_instance.gameObject.transform.SetParent(null, false);
				DontDestroyOnLoad(_instance.gameObject);
			}
		}
		else if (this != _instance)
		{
			Debug.Log("Duplicate Singleton. Remove new " + gameObject);
			DestroyImmediate(gameObject);
		}
		else
		{
			if (dontDestroyOnLoad)
			{
				transform.SetParent(null, false);
				DontDestroyOnLoad(gameObject);
			}
		}
	}

	static T _instance;
	static object _lock = new object();
	public static T Instance
	{
		get
		{
			if (Singleton.applicationIsQuitting)
				return null;

			lock (_lock)
			{
				if (_instance == null)
				{
					_instance = (T)FindObjectOfType(typeof(T));
					if (_instance == null)
					{
						GameObject singleton = new GameObject();
						_instance = singleton.AddComponent<T>();
						singleton.name = typeof(T).ToString() + "(Singleton)";
						DontDestroyOnLoad(singleton);
					}
				}
				return _instance;
			}
		}
	}

	protected virtual void OnApplicationQuit()
	{
		Singleton.applicationIsQuitting = true;
	}
}

internal static class Singleton
{
	internal static bool applicationIsQuitting = false;
}

public class SingletonScriptableObject<T> : ScriptableObject where T : ScriptableObject
{
	static T _instance = null;
	public static T Instance
	{
		get
		{
			if (_instance == null)
			{
				var objects = Resources.FindObjectsOfTypeAll<T>();
				if (objects.Length > 0)
				{
					Debug.Assert(objects.Length == 1, "There are too much assets of type " + typeof(T).FullName);
					_instance = objects[0];
				}
				else
					Debug.LogError("There is no assets of type " + typeof(T).FullName);
			}
			return _instance;
		}
	}
}