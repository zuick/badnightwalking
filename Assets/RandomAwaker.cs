﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAwaker : MonoBehaviour
{

	public int maxDelay = 10;
	public GameObject targetObject;

	void Awake()
	{
		if (targetObject)
		{
			targetObject.SetActive(false);
			StartCoroutine(WakeUp(UnityEngine.Random.Range(0, maxDelay)));
		}

	}

	IEnumerator WakeUp(int delay)
	{
		while (true)
		{
			yield return new WaitForSeconds(delay);
			targetObject.SetActive(true);
		}
	}
}
